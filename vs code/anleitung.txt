

^B	            Seitenleiste
^J	            Terminal einschalten
^+	            Skalierung ändern
^ShiftP         Befehlszeile


Dateisteuerung
---------------
^W              Datei schliessen
^Tab            Zwischen Dateien wechseln
^P              Datei suchen


Editierfunktionen
-----------------

^D              Aktuelles Wort markieren
^Pfeil vor      Nächstes Wort

